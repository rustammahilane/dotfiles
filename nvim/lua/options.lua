vim.opt.number         = true
-- vim.opt.relativenumber = true
vim.opt.autoindent     = true
vim.opt.tabstop        = 4
vim.opt.smarttab       = true
vim.opt.shiftwidth     = 4
vim.opt.softtabstop    = 4
vim.opt.mouse          = "a"
