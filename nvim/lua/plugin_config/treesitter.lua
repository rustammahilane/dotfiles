require("nvim-treesitter.configs").setup ({
	ensure_installed = {'lua', 'c', 'cpp', 'vim'},
	auto_install = true,
	highlight = {
		enable = true,
	},
})
