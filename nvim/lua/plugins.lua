local Plug = vim.fn['plug#']

vim.call('plug#begin')

	-- tagbar on right shows objects, classes in the code
Plug ('preservim/tagbar')
Plug ('nvim-tree/nvim-web-devicons')
Plug 'windwp/nvim-autopairs'
Plug 'nvim-lualine/lualine.nvim'
Plug ('preservim/nerdcommenter')
--themes
--Plug ("rebelot/kanagawa.nvim")
--Plug ("catppuccin/nvim", { ['as'] = 'catppuccin'})
Plug ('ellisonleao/gruvbox.nvim')
Plug ('folke/tokyonight.nvim')
-- On-demand loading
--Plug ('preservim/nerdtree', { ['on'] = 'NERDTreeToggle' })
Plug ('ms-jpq/chadtree', { ['branch'] = 'chad', ['do'] = 'python3 -m chadtree deps'})
--lets you edit in file explorer
Plug 'stevearc/oil.nvim'
	-- plenary is dependancy of telescope
Plug ('nvim-lua/plenary.nvim')
-- Any valid git URL is allowed

-- Multiple Plug commands can be written in a single line using ; separators
Plug ('nvim-treesitter/nvim-treesitter', { ['do'] = ':TSUpdate'})
-- git plugins
Plug ("lewis6991/gitsigns.nvim")
Plug ('tpope/vim-fugitive')
-- Plugin options
Plug ('nvim-telescope/telescope.nvim', { ['tag'] = '0.1.5' })
-- language server providers
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'neovim/nvim-lspconfig'
-- autocomplete and snippets
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/vim-vsnip-integ'

Plug ('iamcco/markdown-preview.nvim', { ['do']= 'cd app && npx --yes yarn install' })


vim.call('plug#end')
