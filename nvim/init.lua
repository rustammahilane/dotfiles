require "keymaps"
require "options"
require "plugins"
require "plugin_config.telescope"
--require "plugin_config.nerdtree"
require "plugin_config.tagbar"
require "plugin_config.nvim_cmp"
require "plugin_config.lsp_config"
require "plugin_config.autopair"
require "plugin_config.devicons"
require "plugin_config.treesitter"
require "plugin_config.lualine"
require "plugin_config.gitsigns"
require "plugin_config.chadtree"

vim.o.background = 'dark'
vim.cmd([[colorscheme gruvbox]])
