This is my AwesomeWM config.

Device: Asus Tuf F15 (Keyboard Controls), use `win+S`  for shortcut.

## Required Packages

* Picom: X11 compositor
* Kitty: terminal emulator
* Polybar
* Rofi: window switcher, app launcher
* Xbacklight: adjust brightness
* Nitrogen(optional)

## Used Apps

* thorium-browser
* Vscodium
* Flameshot: screenshot manager
