
#: Window management {{{

#: New window

map kitty_mod+enter new_window
map cmd+enter       new_window

#::  You can open a new kitty window running an arbitrary program, for
#::  example::

#::      map kitty_mod+y launch mutt

#::  You can open a new window with the current working directory set
#::  to the working directory of the current window using::

#::      map ctrl+alt+enter launch --cwd=current

#::  You can open a new window that is allowed to control kitty via
#::  the kitty remote control facility with launch --allow-remote-
#::  control. Any programs running in that window will be allowed to
#::  control kitty. For example::

#::      map ctrl+enter launch --allow-remote-control some_program

#::  You can open a new window next to the currently active window or
#::  as the first window, with::

#::      map ctrl+n launch --location=neighbor
#::      map ctrl+f launch --location=first

#::  For more details, see launch
#::  <https://sw.kovidgoyal.net/kitty/launch/>.

#: New OS window

map kitty_mod+n new_os_window
map cmd+n       new_os_window

#::  Works like new_window above, except that it opens a top-level OS
#::  window. In particular you can use new_os_window_with_cwd to open
#::  a window with the current working directory.

#: Close window

map kitty_mod+w close_window
map shift+cmd+d close_window

#: Next window

map kitty_mod+] next_window

#: Previous window

map kitty_mod+[ previous_window

#: Move window forward

map kitty_mod+f move_window_forward

#: Move window backward

map kitty_mod+b move_window_backward

#: Move window to top

map kitty_mod+` move_window_to_top

#: Start resizing window

map kitty_mod+r start_resizing_window
map cmd+r       start_resizing_window

#: First window

map kitty_mod+1 first_window
map cmd+1       first_window

#: Second window

map kitty_mod+2 second_window
map cmd+2       second_window

#: Third window

map kitty_mod+3 third_window
map cmd+3       third_window

#: Fourth window

map kitty_mod+4 fourth_window
map cmd+4       fourth_window

#: Fifth window

map kitty_mod+5 fifth_window
map cmd+5       fifth_window

#: Sixth window

map kitty_mod+6 sixth_window
map cmd+6       sixth_window

#: Seventh window

map kitty_mod+7 seventh_window
map cmd+7       seventh_window

#: Eight window

map kitty_mod+8 eighth_window
map cmd+8       eighth_window

#: Ninth window

map kitty_mod+9 ninth_window
map cmd+9       ninth_window

#: Tenth window

map kitty_mod+0 tenth_window

#: Visually select and focus window

map kitty_mod+f7 focus_visible_window

#::  Display overlay numbers and alphabets on the window, and switch
#::  the focus to the window when you press the key. When there are
#::  only two windows, the focus will be switched directly without
#::  displaying the overlay. You can change the overlay characters and
#::  their order with option visual_window_select_characters.

#: Visually swap window with another

map kitty_mod+f8 swap_with_window

#::  Works like focus_visible_window above, but swaps the window.

#: }}}

#: Tab management {{{

#: Next tab

map kitty_mod+right next_tab
map shift+cmd+]     next_tab
map ctrl+tab        next_tab

#: Previous tab

map kitty_mod+left previous_tab
map shift+cmd+[    previous_tab
map ctrl+shift+tab previous_tab

#: New tab

map kitty_mod+t new_tab
map cmd+t       new_tab

#: Close tab

map kitty_mod+q close_tab
map cmd+w       close_tab

#: Close OS window

map shift+cmd+w close_os_window

#: Move tab forward

map kitty_mod+. move_tab_forward

#: Move tab backward

map kitty_mod+, move_tab_backward

#: Set tab title

map kitty_mod+alt+t set_tab_title
map shift+cmd+i     set_tab_title


#: You can also create shortcuts to go to specific tabs, with 1 being
#: the first tab, 2 the second tab and -1 being the previously active
#: tab, and any number larger than the last tab being the last tab::

#:     map ctrl+alt+1 goto_tab 1
#:     map ctrl+alt+2 goto_tab 2

#: Just as with new_window above, you can also pass the name of
#: arbitrary commands to run when using new_tab and new_tab_with_cwd.
#: Finally, if you want the new tab to open next to the current tab
#: rather than at the end of the tabs list, use::

#:     map ctrl+t new_tab !neighbor [optional cmd to run]
#: }}}
